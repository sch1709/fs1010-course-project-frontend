import React, { useState } from 'react'
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, Container } from 'reactstrap'
import { NavLink as RouteLink } from 'react-router-dom'

const Navigation = () => {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = () => setIsOpen(!isOpen)

    const checkActive = (match, location) => {
        if(!location) return false;
        const {pathname} = location;
        return pathname === "/";
    }

    return (
        <Navbar light color="white"  expand="md" fixed="top">
            <Container>
            <NavbarBrand href="/">&lt; <b>S</b>C /&gt;</NavbarBrand>
            <NavbarToggler onClick={toggle} />
            <Collapse isOpen={isOpen} navbar>
                <Nav className="ml-auto" navbar>
                    <NavItem className="navEffect">
                        <NavLink activeClassName="active" isActive={checkActive} style={{paddingLeft:"0", paddingRight:"0"}} tag={RouteLink} to="/">Home</NavLink>
                    </NavItem>
                    <NavItem className="navEffect">
                        <NavLink activeClassName="active" style={{paddingLeft:"0", paddingRight:"0"}} tag={RouteLink} to="/resume">Resume</NavLink>
                    </NavItem>
                    <NavItem className="navEffect">
                        <NavLink activeClassName="active" style={{paddingLeft:"0", paddingRight:"0"}} tag={RouteLink} to="/portfolio">Portfolio</NavLink>
                    </NavItem>
                    <NavItem className="navEffect">
                       <NavLink activeClassName="active" style={{paddingLeft:"0", paddingRight:"0"}} tag={RouteLink} to="/contact">Contact Us</NavLink>
                    </NavItem>
                    <NavItem className="navEffect">
                        <NavLink activeClassName="active" style={{paddingLeft:"0", paddingRight:"0"}} tag={RouteLink} to="/submissions">Sign In</NavLink>
                    </NavItem>
                    <NavItem className="navEffect">
                        <NavLink activeClassName="active" style={{paddingLeft:"0", paddingRight:"0"}} tag={RouteLink} to="/singup">Sing Up</NavLink>
                    </NavItem>
                </Nav>
            </Collapse>
            </Container>
        </Navbar>
    )
}

export default Navigation