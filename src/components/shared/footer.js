import React from 'react'
import { Container } from 'reactstrap'

const Footer = () => {
    return(
    <footer style={{position:"fixed", bottom:"0", right:"0"}} className="py-4 bg-dark">
        <Container>
            <p className="m-0 text-center text-white footer">All rights reserved to Samuel Cruces || 2021</p>
        </Container>
    </footer>
  )
}

export default Footer