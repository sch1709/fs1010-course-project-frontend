import React, { useState } from 'react'
import { Container, Col, Button, Form, FormGroup, Label, Input, Card, CardBody, CardHeader } from 'reactstrap'
import { useHistory, useLocation } from 'react-router-dom'
import padlock from '../../public/img/padlock.png' 
import loginForm from '../../public/img/login-form.jpg'

const Login = () => {
    let history = useHistory();
    let location = useLocation();
    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const loginSubmit = async event => {
        
        event.preventDefault()
        const response = await fetch('http://localhost:4000/auth', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({username, password})
        })
        const payload = await response.json()
        if (response.status >= 400) {
          alert("Invalid credentials | Your username or/and your password are not correct, please try again.")
        } else {
          sessionStorage.setItem('token', payload.token)

          let { from } = location.state || { from: { pathname: "/" } };
          history.replace(from);
        }
    }

    return (
        <Container className="login">
        <Col sm={{size: 4, offset: 4}}>
        <Card className="blur my-5" style={{backgroundImage:`url(${loginForm })`, backgroundSize:"cover", backgroundPosition:"center", boxShadow:"5px 10px 18px #888888"}}>
          <CardHeader>
            <img className="cardImage" src={padlock} alt="padlock" />
          </CardHeader>
          <CardBody>
            <h3><strong>Sign</strong> In</h3>
            <p>Welcome back! sign in to continue...</p>
            <Form className="my-2" onSubmit={loginSubmit}>
              <FormGroup>
                <Label for="usernameEntry">Username</Label>
                <Input className="darkPlaceholder" type="text" name="username" id="usernameEntry" placeholder="Username" value={username} onChange={e => setUsername(e.target.value)} />
              </FormGroup>
              <FormGroup>
                <Label for="passwordEntry">Password</Label>
                <Input className="darkPlaceholder"  type="password" name="password" id="passwordEntry" placeholder="Valid password" onChange={e => setPassword(e.target.value)} />
              </FormGroup>
              <Button style={{float:"right", width:"100%", marginTop: "15px", fontWeight:"900" }} outline color="dark">Sign in</Button>
            </Form>
          </CardBody>
        </Card>
        </Col>
      </Container>
    )
}

export default Login