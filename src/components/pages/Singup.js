import React, { useState } from 'react'
import { Container, Col, Form, FormGroup, FormText, Label, Input, Button, Card, CardBody, CardHeader } from 'reactstrap'
import signup from '../../public/img/signup.png'
import signupForm from '../../public/img/signup-form.jpg'

const Singup = () => {
    const[email, setEmail] = useState("")
    const[username, setUsername] = useState("")
    const[password, setPassword] = useState("")

    const handleSubmit = async (e) => {
        e.preventDefault()
        if(password.length < 8){
            alert('Oops the password should be more than 8 characters!')
        }else{
            const response = await fetch('http://localhost:4000/users', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({email, username, password})
            })
            const payload = await response.json()
            if (response.status === 400) {
                alert(`Oops! Error: ${payload.message} for fields: ${payload.invalid.join(",")}`)
            }else if(response.status === 409){
                if(payload === "mail"){
                    alert("Oops! Error: this email is already in our database.")
                }else{
                    alert("Oops! Error: this username already exist")
                }
            }else{
                alert(`User created successfully id: ${payload.username}`)
                setEmail('')
                setUsername('')
                setPassword('')
            }
        }
    }

    return(
        <Container>
            <Col sm={{size: 4, offset: 4}}>
                <Card style={{ marginBottom:"30px", backgroundImage:`url(${signupForm})`, backgroundSize:"cover", backgroundPosition:"center", boxShadow:"5px 10px 18px #888888"}}>
                    <CardHeader> 
                        <img className="cardImage" src={signup} alt="add user" />
                    </CardHeader>
                    <CardBody style={{color:"#fff"}}>
                        <h3><strong>Sign</strong> Up</h3>
                        <p>Create a new account!</p>
                        <Form className="my-2" onSubmit={handleSubmit}>
                            <FormGroup>
                                <Label sm={12}>Email</Label>
                                <Col sm={12}>
                                    <Input className="lightPlaceholder" type="email" name="email" id="emailUser" placeholder="Enter email" required value={email} onChange={e => setEmail(e.target.value)} />
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Label sm={12}>Username</Label>
                                <Col sm={12}>
                                    <Input className="lightPlaceholder" type="text" name="username" id="usernameUser" placeholder="Enter a username" required value={username} onChange={e => setUsername(e.target.value)} />
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Label sm={12}>Password</Label>
                                <Col sm={12}>
                                    <Input className="lightPlaceholder" type="password" name="password" id="userPassword" placeholder="Enter a password" required value={password} onChange={e => setPassword(e.target.value)} />
                                    <FormText color="light"><strong>*</strong>Password must have 8 characters at least</FormText>
                                </Col>
                            </FormGroup>
                            <FormGroup>
                                <Col>
                                    <Button style={{float:"right", width:"100%", marginTop: "15px", fontWeight:"900"}} outline color="light">Sign Up</Button>
                                </Col>
                            </FormGroup>
                        </Form>
                    </CardBody>
                </Card>
            </Col>
        </Container>
    )
}

export default Singup