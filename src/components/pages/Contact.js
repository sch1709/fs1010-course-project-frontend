import React, { useState } from 'react'
import { Form, FormGroup, Col, Input, Label, Button, Container, CardBody, Card } from 'reactstrap'
import contact from '../../public/img/contact.png'


const Contact = () => {
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [phoneNumber, setPhoneNumber] = useState("")
    const [content, setContent] = useState("")

    const formSubmit = async event => {
        event.preventDefault()
        const response = await fetch('http://localhost:4000/contact_form/entries', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({name, email, phoneNumber, content})
        })
        const payload = await response.json()
        if (response.status >= 400) {
            alert(`Oops! Error: ${payload.message} for fields: ${payload.invalid.join(",")}`)
        } else {
            alert(`Congrats! Submission submitted with id: ${payload.id}`)
        }
        setName('')
        setEmail('')
        setPhoneNumber('')
        setContent('')
    }

    return (
        <Container style={{backgroundImage:`url(${contact})`, backgroundRepeat:"no-repeat", backgroundSize:"50%", backgroundPosition:"right, center"}}>
            <h1>Let's <strong>talk!</strong></h1>
            <p>Use form to reach me, I'll get back to you within 24 hours!</p>
            <Card style={{marginBottom:"30px", width:"450px", }}>
                <CardBody>
                    <Form className="my-5" onSubmit={formSubmit}>
                        <FormGroup row>
                            <Label for="emailEntry" sm={4}>Email</Label>
                            <Col sm={8}>
                                <Input type="email" name="email" id="emailEntry" placeholder="Enter email to contact" required value={email} onChange={e => setEmail(e.target.value)} />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="phoneEntry" sm={4}>Phone Number</Label>
                            <Col sm={8}>
                                <Input type="phone" name="phone" id="phoneEntry" placeholder="Enter phone number" value={phoneNumber} onChange={e => setPhoneNumber(e.target.value)} />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="nameEntry" sm={4}>Full Name</Label>
                            <Col sm={8}>
                                <Input type="name" name="name" id="nameEntry" placeholder="Enter your full name" required value={name} onChange={e => setName(e.target.value)} />
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="messageEntry" sm={4}>Message</Label>
                            <Col sm={8}>
                                <Input style={{resize:"none", height:"100px"}} type="textarea" name="text" id="messageEntry" required value={content} onChange={e => setContent(e.target.value)} />
                            </Col>
                        </FormGroup>
                        <FormGroup check row>
                            <Col sm={12}>
                                <Button style={{float:"right", width:"100px"}} outline color="info">Submit</Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </CardBody>
            </Card>
        </Container>
      )
    }

    export default Contact