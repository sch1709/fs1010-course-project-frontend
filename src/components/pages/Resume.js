import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome' 
import { faHtml5, faCss3Alt, faJs, faReact, faNode, faPython, faJava, faCuttlefish } from '@fortawesome/free-brands-svg-icons'

const Resume = () => {
    return (
        <Container>
        <Row className="my-3"> 
            <Col sm="12">
                <h1 style={{lineHeight:"15px"}}>Career <strong>Objective</strong></h1>
                <p style={{color:"#999999",}}>Working on it.</p>
                <p>Develop through my education and work experience as Full Stack, tools able to facilitate with creativity and innovation the work of the user, in addition to constantly learn in order to improve.</p>
            </Col>
        </Row>
        <Row style={{marginBottom:"60px"}}>
            <Col sm="3">
                <h3 className="mb-4">Soft Skills</h3>
                <ul style={{listStyle:"none"}}>
                    <li>Communication</li>
                    <li>Creativity</li>
                    <li>Planning</li>
                    <li>Problem solving</li>
                    <li>Milti-tasking</li>
                    <li>Team-work</li>
                </ul>
                <h3 className="mb-4">Programming Skills</h3>
                <ul style={{listStyle:"none"}}>
                    <li className="mb-3">
                        <FontAwesomeIcon style={{fontSize:"1.5rem", color:"#D94925"}} icon={faHtml5}/><span style={{color:"#999"}}>&nbsp; HTML | Expert</span>
                        <div style={{width:"200px", height:"10px", border:"1px solid #333333", background:"linear-gradient(to right, #283E51, #4B79A1)"}}></div>
                    </li> 
                    <li className="mb-3">
                        <FontAwesomeIcon style={{fontSize:"1.5rem", color:"#146CAD"}} icon={faCss3Alt} /><span style={{color:"#999"}}>&nbsp; CSS | Expert</span>
                        <div style={{width:"200px", height:"10px", border:"1px solid #333", background:"linear-gradient(to right, #283E51, #4B79A1)"}}></div>
                    </li> 
                    <li className="mb-3">
                    <FontAwesomeIcon style={{fontSize:"1.5rem", color:"#E5D04A"}} icon={faJs} /><span style={{color:"#999"}}>&nbsp; JavaScript | Advanced</span>
                        <div style={{width:"200px", height:"10px", border:"1px solid #333", background:"linear-gradient(to right, #283E51, #4B79A1 75%, #fff 75% 100%)"}}></div>
                    </li>
                    <li className="mb-3">
                    <FontAwesomeIcon style={{fontSize:"1.5rem", color:"#76D5F1"}} icon={faReact} /><span style={{color:"#999"}}>&nbsp; React | Intermediate</span>
                        <div style={{width:"200px", height:"10px", border:"1px solid #333333", background:"linear-gradient(to right, #283E51, #4B79A1 50%, #fff 50% 100%)"}}></div>
                    </li> 
                    <li className="mb-3">
                    <FontAwesomeIcon style={{fontSize:"1.5rem", color:"#397F38"}} icon={faNode} /><span style={{color:"#999"}}>&nbsp; Node | Intermediate</span>
                        <div style={{width:"200px", height:"10px", border:"1px solid #333333", background:"linear-gradient(to right, #283E51, #4B79A1 50%, #fff 50% 100%)"}}></div>
                    </li> 
                    <li className="mb-3">
                    <FontAwesomeIcon style={{fontSize:"1.5rem", color:"#4B8BBE"}} icon={faPython} /><span style={{color:"#999"}}>&nbsp; Python | Intermediate</span>
                        <div style={{width:"200px", height:"10px", border:"1px solid #333333", background:"linear-gradient(to right, #283E51, #4B79A1 50%, #fff 50% 100%)"}}></div>
                    </li> 
                    <li className="mb-3">
                    <FontAwesomeIcon style={{fontSize:"1.5rem", color:"#E8292C"}} icon={faJava} /><span style={{color:"#999"}}>&nbsp; Java | Intermediate</span>
                        <div style={{width:"200px", height:"10px", border:"1px solid #333333", background:"linear-gradient(to right, #283E51, #4B79A1 50%, #fff 50% 100%)"}}></div>
                    </li>
                    <li className="mb-3">
                    <FontAwesomeIcon style={{fontSize:"1.5rem", color:"#6092C7"}} icon={faCuttlefish} /><span style={{color:"#999"}}>&nbsp; C | Intermediate</span>
                        <div style={{width:"200px", height:"10px", border:"1px solid #333333", background:"linear-gradient(to right, #283E51, #4B79A1 50%, #fff 50% 100%)"}}></div>
                    </li>
                </ul>
                <h3 className="mt-5 mb-3">Complementary Skills</h3>
                <ul style={{listStyle:"none", display:"block"}}>
                    <li>Git</li>
                    <li>Sass</li>
                    <li>Bootstrap</li>
                    <li>Skeleton</li>
                    <li>Photoshop</li>
                    <li>Mockflow Wireframe</li>
                    <li>SEO</li>
                </ul>
            </Col>
            <Col sm="9">
                <h2 style={{borderTop:"1px solid #333", borderBottom:"1px solid #333"}}>Professional Experience</h2>
                <h4>Freelance</h4>
                <p style={{color:"#999", lineHeight:"5px"}}>Dic 2015 - Present</p>
                <p style={{textAlign:"justify", fontWeight:"bold"}}>Worked as a team member or by my own to create a number of apps and participated in a number of web projects</p>
                <ul style={{listStyle:"square", textAlign:"justify", marginBottom:"30px"}}>
                    <li>Designed and developed the user interface upgrade using React in the administrative control panel, with this we improved the performance of the app.</li>
                    <li>Designed and developed a front end web app which was used for  administration, and invoice control of a group of gas  stations with HTML, Sass(CSS), Bootstrap and JavaScript.</li>
                    <li>Designed and developed the front end of the point-of-sales app restaraurants.</li>
                    <li>Developed a reporting module for generating administrative reports based on data which the clients created when using the app.</li>
                    <li>Designed, developed and maintained websites for other clients.</li>
                </ul>
                <h3>Consorcio Luna SA de CV</h3>
                <h4>Front End developer</h4>
                <p style={{color:"#999", lineHeight:"5px"}}>Aug 2007 - Dic 2015</p>
                <p style={{textAlign:"justify", fontWeight:"bold"}}>Worked as a webmaster creating and maintaining web app for the brands. Sometimes I work as a IT consultant for this company yet.</p>
                <ul style={{listStyle:"square", textAlign:"justify", marginBottom:"30px"}}>
                    <li>Designed and Developed the websites for the different brands of the company with HTML, CSSS, Javascript, JQuery and PHP for the forms.</li>
                    <li>Design the SEO web positioning plan through the analysis of the competition, search engine activity, social networks with Semrush, Google Adwords, Google Search Engine, Google Business, Metricool and Similar Web and developed the keywords campaign for different brands of the company.</li>
                    <li>Purchased the computer equipment and technology for the   company supported with a benchmarking and realized a cost saving of 35% for the budget.</li>
                    <li>Conducted a preventive and corrective maintenance program for the company’s computer equipment.</li>
                    <li>Rewrote web code for the company and their official brands template websites, improved the performance.</li>
                </ul>

                <h2 style={{borderTop:"1px solid #333", borderBottom:"1px solid #333"}}>Education</h2>
                <h4>Computer Engineering</h4>
                <p style={{color:"#999", lineHeight:"5px"}}>Aug 2005 - May 2009</p>
                <p style={{textAlign:"justify", fontWeight:"bold"}}>Universidad del Valle de México | Four years of undergraduate study.</p>
                <h4>Courses</h4>
                <ul style={{listStyle:"square"}}>
                    <li>React the complete guide |<span style={{color:"#999"}}> Udemy</span> | <span style={{color:"#999"}}>Online</span></li>
                    <li>Introduction Programming Python |<span style={{color:"#999"}}> Coursera</span> | <span style={{color:"#999"}}>Online</span></li>
                    <li>Bootstrap, PHP |<span style={{color:"#999"}}> Udemy</span> | <span style={{color:"#999"}}>Online</span></li>
                    <li>Learning Pentesting |<span style={{color:"#999"}}> Udemy</span> | <span style={{color:"#999"}}>Online</span></li>
                    <li>English ESL |<span style={{color:"#999"}}> George Brown College</span> | <span style={{color:"#999"}}>Aug 2003 - Dic 2003</span></li>
                </ul>
            </Col>
        </Row>
    </Container>
    )
}

export default Resume