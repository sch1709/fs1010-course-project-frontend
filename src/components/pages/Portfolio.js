import React from 'react'
import { Container, Row, Col } from 'reactstrap'
import novuspack from '../../public/img/novuspack.jpg'
import marine from '../../public/img/marine.jpg'
import dira from '../../public/img/dira.jpg'
import automotive from '../../public/img/automotive.jpg'
import raa from '../../public/img/raa.jpg'
import central from '../../public/img/central.jpg'
import furi from '../../public/img/furi.jpg'

const Portfolio = () => {

    const images = [
        {
            name: 'novuspack',
            location: novuspack,
            url: 'http://novuspack.com/'
        },
        {
            name: 'marine',
            location: marine,
            url: 'https://anchorsawaymarinesurveys.com/'
        },
        {
            name: 'dira',
            location: dira,
            url: 'http://dira.com.mx/'
        },
        {
            name: 'automotive',
            location: automotive,
            url: 'https://www.automotiveappraisals.ca/'
        },
        {
            name: 'raa',
            location: raa,
            url: 'https://raa.com.mx/'
        },
        {
            name: 'central',
            location: central,
            url: 'https://github.com/sch1709/CENTRAL_CENTRAL_QRO'
        },
        {
            name: 'furi',
            location: furi,
            url: 'https://github.com/sch1709/furi' 
        }

    ]

    return(
        <Container>
           <Row className="my-3"> 
                <Col sm="12">
                    <h1 style={{lineHeight:"15px"}}>Some<strong>Projects</strong></h1>
                    <p style={{color:"#999999",}}>My portfolio.</p>
                    <p>I'm appreciate the trust of the clients, this is just some of my projects. Please feel free to click in the image and <strong>go to the site.</strong></p>
                </Col>
            </Row>
            <Row style={{marginBottom:"60px"}}>
                <ul style={{listStyle:"none"}}>
                    {
                        images.map((image, index) => 
                            <li key={index} style={{display:"inline", padding:"30px", lineHeight:"300px"}}>
                                <a href={image.url} target="_blank" rel="noreferrer">
                                    <img className="image" src={image.location} alt={image.name}/>
                                </a>
                            </li>
                        )
                    }
                </ul>
            </Row>
        </Container>
    )
}

export default Portfolio