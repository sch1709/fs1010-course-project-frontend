import React from 'react'
import { Container, Row, Col, CardBody, CardTitle, Card } from 'reactstrap'
import quotes from '../../public/img/quotes.png'
import home from '../../public/img/home.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome' 
import { faLayerGroup, faPencilAlt, faChartLine } from '@fortawesome/free-solid-svg-icons'


const Home = () => {
    return(
        <Container>
            <Row className="my-5 py-4">
                <Col lg="7">
                    <img className="img-fluid mb-4 mb-lg-0" src={home} alt="home" />
                </Col>
                <Col lg="5">
                    <h1 style={{lineHeight:"15px"}}>I'm <strong>Samuel</strong> Cruces</h1>
                    <p style={{color:"#999999",}}>Full Stack Web Developer | UI/UX </p>
                    <p className="quote">
                        <img src={quotes} style={{height:"30px", transform:"Rotate(180deg)"}} alt="quotes"/>
                        &nbsp; My passion is to create interactive digital experiences on the web through solid codes. &nbsp;
                        <img src={quotes} style={{height:"30px"}} alt="quotes" />
                    </p>
                </Col>
            </Row>
            <Row className="py-5">
                <h1><span style={{color: "#247ae3"}}>&#123;</span> <strong>HELLO</strong> WORLD! <span style={{color: "#247ae3"}}>&#125;</span></h1>
                <p style={{textAlign:"justify"}}>I am a web developer based in Brantford, Canada interested in creating really new interactive user experiences. I studied computer engineering at the University of - Valle de México -, then I worked as a freelance for a long time developing some websites and applications in my country. Now I start a new adventure with my family by moving to Canada.</p>
            </Row>
            <Row className="mb-5">
                <Col md="4" className="mb-5">
                    <Card>
                        <CardBody>
                            <FontAwesomeIcon className="cardIcon" style={{color: "#363778"}} icon={faLayerGroup}/>
                            <CardTitle><h2 style={{textAlign:"center", marginTop:"10px"}}>Full Stack</h2></CardTitle>
                            <CardBody style={{textAlign:"justify", textIndent:"30px"}}>As a full stack I can help you in your project since the first step up to the end. It will be my pleasure assitence you in all the process and give you an advise wich tech are better for you particular case in order to create a succesfull app. </CardBody>
                        </CardBody>
                    </Card>
                </Col>
                <Col md="4" className="mb-5">
                    <Card>
                        <CardBody>
                            <FontAwesomeIcon className="cardIcon" style={{color:"#f5725c"}} icon={faPencilAlt}/>
                            <CardTitle><h2 style={{textAlign:"center", marginTop:"10px"}}>UX | UI Design</h2></CardTitle>
                            <CardBody style={{textAlign:"justify", textIndent:"30px"}}>Let's create an awesome app who the user feel confortable and have a real awesome experience every time that enter to your site or app, make the user the center part of you project and interactive with them easily.</CardBody>
                        </CardBody>
                    </Card>
                </Col>
                <Col md="4" className="mb-5">
                    <Card>
                        <CardBody>
                            <FontAwesomeIcon className="cardIcon" style={{color:"#e53f6f"}} icon={faChartLine}/>
                            <CardTitle><h2 style={{textAlign:"center", marginTop:"10px"}}>SEO</h2></CardTitle>
                            <CardBody style={{textAlign:"justify", textIndent:"30px"}}>Your site or app can look incredible and works really well but just a few people can see your amazing job, I can help you with this, through differents tools and test we can push your app to the top in the search engines.</CardBody>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}

export default Home